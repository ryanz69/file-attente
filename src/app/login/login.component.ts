import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { adminService } from '../service/admin.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  loginForm: FormGroup;
  error = false;

  constructor(private adminService: adminService, private formBuilder: FormBuilder) { }

  ngOnInit() {
    this.logForm();
  }

  logForm() {
    this.loginForm = this.formBuilder.group({
      username: ['', Validators.required],
      password: ['', Validators.required],
    })
  }

  onSubmitForm() {
    let value = this.loginForm.value;
    this.adminService.login(value).subscribe(
      () => {},
      () => this.error = true
    );
  }

}
