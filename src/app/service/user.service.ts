import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from 'src/environments/environment';

export interface Request {
name: string,
}

@Injectable({
providedIn: 'root'
})
export class userService {

  constructor(private http: HttpClient) { }

  public getAll() {
    return this.http.get<Request[]>(environment.backUrl + "/api/user/");
  }

  public add(entity) {
    return this.http.post<Request[]>(environment.backUrl + '/api/user/add', entity);
  }

  public remove(id) {
    return this.http.delete(environment.backUrl + '/api/user/admin/remove/' + id)
  }
}
