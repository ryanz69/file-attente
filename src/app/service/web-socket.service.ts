import { Injectable } from '@angular/core';
import * as io from 'socket.io-client';
import { Observable, Subject } from 'rxjs';
import { environment } from '../../environments/environment';

@Injectable({
  providedIn: 'root'
})
export class WebSocketService {

  private socket;

  constructor() { }

  getUserList() { 
    let observable = new Observable(observer => { 
      this.socket = io(environment.ws_url); 
      this.socket.on('help-list', (data) => observer.next(data)); 
        return () => { this.socket.disconnect(); }; 
      }) 
    return observable;
  } 

}
