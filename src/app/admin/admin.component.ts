import { Component, OnInit, OnDestroy } from '@angular/core';
import { userService } from '../service/user.service';
import { adminService } from '../service/admin.service';
import { WebSocketService } from '../service/web-socket.service';

@Component({
  selector: 'app-admin',
  templateUrl: './admin.component.html',
  styleUrls: ['./admin.component.css']
})
export class AdminComponent implements OnInit {
  allUser;
  connection;

  constructor(private userService: userService, private adminService: adminService, private webSocket: WebSocketService) { }

  ngOnInit() {
    this.connection = this.webSocket.getUserList().subscribe(date => this.allUser = date);
  }

  ngOnDestroy() { 
    this.connection.unsubscribe(); 
  }

  remove(id) {
    this.userService.remove(id).subscribe(
      (response) => console.log(response),
      (error) => console.log(error)
    );
  }

  logout() {
    this.adminService.logout();
  }

}
