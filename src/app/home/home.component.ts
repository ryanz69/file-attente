import { Component, OnInit, OnDestroy } from '@angular/core';
import { userService } from '../service/user.service';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { WebSocketService } from '../service/web-socket.service';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {
  allUser;
  loginModal = false;
  connection;
  helpForm: FormGroup

  constructor(private userService: userService, private formBuilder: FormBuilder, private webSocket: WebSocketService) {}

  ngOnInit() {
    this.connection = this.webSocket.getUserList().subscribe(date => this.allUser = date)
    this.hlpForm();
  }

  ngOnDestroy() { 
    this.connection.unsubscribe(); 
  }

  hlpForm() {
    this.helpForm = this.formBuilder.group({
      name: ['', Validators.required],
    })
  }

  onSubmitForm() {
    let value = this.helpForm.value;
    this.userService.add(value).subscribe(
      () => {},
      (error) => console.log(error)
    );
  }
}
